package GaborKovac.M3TestVezbanjeSprint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class M3TestVezbanjeSprintApplication {

	public static void main(String[] args) {
		SpringApplication.run(M3TestVezbanjeSprintApplication.class, args);
	}

}
