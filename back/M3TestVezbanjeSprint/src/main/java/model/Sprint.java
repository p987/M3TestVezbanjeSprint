package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Sprint {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String Ime;
	
	@Column
	private String ukupnoBodova;
	
	@OneToMany
	private List<Zadatak> zadatakLista  = new ArrayList<Zadatak>();
	
	
	public Sprint() {
		super();
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getIme() {
		return Ime;
	}


	public void setIme(String ime) {
		Ime = ime;
	}


	public String getUkupnoBodova() {
		return ukupnoBodova;
	}


	public void setUkupnoBodova(String ukupnoBodova) {
		this.ukupnoBodova = ukupnoBodova;
	}
	
	
}
