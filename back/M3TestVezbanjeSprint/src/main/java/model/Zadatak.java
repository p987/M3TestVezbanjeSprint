package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Zadatak {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, unique = true)
	private String ime;
	
	@Column(nullable = false)
	private String zaduzeni;
	
	@Column
	private int bodovi;
	
	@ManyToOne
	private Sprint sprint;
	
	@ManyToOne
	private Stanje stanje;
}
